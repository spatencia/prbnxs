﻿using ApiNexos.Interfaces;
using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiNexos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutorController : ControllerBase
    {
        private readonly IAutorServices _IAutorS;

        public AutorController(IAutorServices iAutorS)
        {
            _IAutorS = iAutorS;
        }

        // GET: api/<AutorController>
        [HttpGet]
        public IActionResult Get()
        {
            Response res = null;
            try
            {
                IEnumerable<Autor> list = _IAutorS.GetAll();
                if (list.Count() > 0)
                    res = new Response(true, "Se encontro información.", list);
                else
                    res = new Response(false, "No se encontro información.");
            }
            catch (Exception ex)
            {
                res = new Response(false, "En este momento no se encuentra disponible el servicio.");
            }
            return Ok(res);
        }

        // GET api/<AutorController>/nombre
        [HttpGet("{nombreCompleto}")]
        public IActionResult Get(string nombreCompleto)
        {
            Response res = null;
            try
            {
                Autor autor = _IAutorS.GetByNombre(nombreCompleto);
                if (autor != null)
                    res = new Response(true, "Se encontro información", autor);
                else
                    res = new Response(false, "No se encontro información");

                return Ok();
            }
            catch (Exception ex)
            {
                res = new Response(false, "En este momento no se encuentra disponible el servicio.");
            }
            return Ok(res);
        }

        // POST api/<AutorController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Autor autor)
        {
            Response res = null;
            try
            {
                res = await _IAutorS.Add(autor);
            }
            catch (Exception)
            {
                res = new Response(false, "Ocurrio un errpr realizando la operación.");
            }
            return Ok(res);
        }


    }
}

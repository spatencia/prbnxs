﻿using ApiNexos.Interfaces;
using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiNexos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EditorialController : ControllerBase
    {
        private readonly IEditorialServices _IEdiS;

        public EditorialController(IEditorialServices iAutorS)
        {
            _IEdiS = iAutorS;
        }

        // GET: api/<AutorController>
        [HttpGet]
        public IActionResult Get()
        {
            Response res = null;
            try
            {
                IEnumerable<Editorial> lista = _IEdiS.GetAll();
                if (lista != null && lista.Count() > 0)
                    res = new Response(true, "Se encontro información.", lista);
                else
                    res = new Response(false, "No se encontro información.");
            }
            catch (Exception ex)
            {
                res = new Response(false, "En este momento no se encuentra disponible el servicio.");
            }
            return Ok(res);
        }

        // GET api/<AutorController>/nombre
        [HttpGet("{nombre}")]
        public IActionResult Get(string nombre)
        {
            Response res = null;
            try
            {
                Editorial edi = _IEdiS.GetByNombre(nombre);
                if (edi != null)
                    res = new Response(true, "Se encontro información.", edi);
                else
                    res = new Response(false, "No se encontro información.");
            }
            catch (Exception ex)
            {
                res = new Response(false,"En este momento no se encuentra disponible el servicio.");
            }
            return Ok(res);
        }

        // POST api/<AutorController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Editorial edi)
        {
            Response res = null;
            try
            {
                res = await _IEdiS.Add(edi);
            }
            catch (Exception)
            {
                res = new Response(false, "Ocurrio un errpr realizando la operación.");
            }
            return Ok(res);
        }

    }
}

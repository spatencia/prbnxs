﻿using ApiNexos.Interfaces;
using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ApiNexos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibroController : ControllerBase
    {
        private readonly ILibroServices _ILibroS;

        public LibroController(ILibroServices iLibroS)
        {
            _ILibroS = iLibroS;
        }

        // GET: api/<AutorController>
        [HttpGet]
        public IActionResult Get()
        {
            Response res = null;
            try
            {
                IEnumerable<Libro> lista = _ILibroS.GetAll();
                if (lista != null && lista.Count() > 0)
                    res = new Response(true, "Se encontro información", lista);
                else
                    res = new Response(false, "No se encontro información");
            }
            catch (Exception ex)
            {
                res = new Response(false, "En este momento no se encuentra disponible el servicio.");
            }
            return Ok(res);
        }

        // GET api/<AutorController>/buscar
        [HttpGet("{buscar}")]
        public IActionResult Get(string buscar)
        {
            Response res = null;
            try
            {
                IEnumerable<Libro> lista = _ILibroS.GetLibrosByAutorTituloAnio(buscar);
                if (lista != null && lista.Count() > 0)
                    res = new Response(true, "Se encontro información", lista);
                else
                    res = new Response(false, "No se encontro información");
            }
            catch (Exception ex)
            {
                res = new Response(false, "En este momento no se encuentra disponible el servicio.");
            }
            return Ok(res);
        }

        // POST api/<AutorController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Libro lib)
        {
            Response res = null;
            try
            {
                res = await _ILibroS.Add(lib);
            }
            catch (Exception)
            {
                res = new Response(false, "Ocurrio un errpr realizando la operación.");
            }
            return Ok(res);
        }

    }
}

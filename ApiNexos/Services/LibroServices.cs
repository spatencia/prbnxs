﻿using ApiNexos.Context;
using ApiNexos.Interfaces;
using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Services
{
    public class LibroServices : ILibroServices
    {
        private readonly ApplicationDbContext db;
        private readonly IMapper _mapper;

        public LibroServices(ApplicationDbContext db, IMapper mapper)
        {
            this.db = db;
            _mapper = mapper;
        }

        public IEnumerable<Libro> GetAll()
        {
            IEnumerable<Libro> ListaLibro = new List<Libro>();
            try
            {
                ListaLibro = (from lb in db.Libro
                              join ed in db.Editorial on lb.IdEditorial equals ed.Id
                              join au in db.Autor on lb.IdAutor equals au.Id
                              select new Libro()
                              {
                                  Id = lb.Id,
                                  Titulo = lb.Titulo,
                                  Anio = lb.Anio,
                                  Genero = lb.Genero,
                                  NumeroPaginas = lb.NumeroPaginas,
                                  IdEditorial = lb.IdEditorial,
                                  NombreEditorial = ed.Nombre,
                                  IdAutor = lb.IdAutor,
                                  NombreAutor = au.NombreCompleto
                              });
            }
            catch (Exception ex)
            {
                throw;
            }
            return ListaLibro;
        }

        public IEnumerable<Libro> GetLibrosByAutorTituloAnio(string buscar)
        {
            IEnumerable<Libro> listado = null;
            try
            {
                listado = (from lb in db.Libro
                           join ed in db.Editorial on lb.IdEditorial equals ed.Id
                           join au in db.Autor on lb.IdAutor equals au.Id
                           where au.NombreCompleto.Contains(buscar) || lb.Titulo.Contains(buscar) || lb.Anio.ToString().Contains(buscar)
                           select new Libro()
                           {
                               Id = lb.Id,
                               Titulo = lb.Titulo,
                               Anio = lb.Anio,
                               Genero = lb.Genero,
                               NumeroPaginas = lb.NumeroPaginas,
                               IdEditorial = lb.IdEditorial,
                               NombreEditorial = ed.Nombre,
                               IdAutor = lb.IdAutor,
                               NombreAutor = au.NombreCompleto
                           });
            }
            catch (Exception ex)
            {
                throw;
            }
            return listado;
        }

        public async Task<Response> Add(Libro libro)
        {
            Response save = null;
            try
            {
                var Editorial = db.Editorial.Where(x => x.Id == libro.IdEditorial).FirstOrDefault();
                if (Editorial is null)
                    return new Response(false, "La editorial no está registrada.");

                var Autor = db.Autor.Where(x => x.Id == libro.IdAutor).FirstOrDefault();
                if (Autor is null)
                    return new Response(false, "El autor no está registrado.");

                if (Editorial.MaximoLibrosRegistrados != -1)
                {
                    int CantLibrosRegistrados = db.Libro.Where(x => x.IdEditorial == Editorial.Id).Count();
                    if (CantLibrosRegistrados >= Editorial.MaximoLibrosRegistrados)
                    {
                        throw new Exception("No es posible registrar el libro, se alcanzó el máximo permitido.");
                    }
                }

                Models.Entities.Libro lib = new Models.Entities.Libro();
                _mapper.Map(libro, lib);
                lib.IdAutor = Autor.Id;
                lib.IdEditorial = Editorial.Id;
                
                db.Libro.Add(lib);
                if (await db.SaveChangesAsync() > 0) save = new Response(true, "Se guardo la información exitosamente.");
                else save = new Response(false, "No se guardo la información.");
            }
            catch (Exception ex)
            {
                save = new Response(false, ex.Message);
            }
            return save;
        }

    }
}

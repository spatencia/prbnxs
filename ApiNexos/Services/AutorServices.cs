﻿using ApiNexos.Context;
using ApiNexos.Interfaces;
using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Services
{
    public class AutorServices : IAutorServices
    {
        private readonly ApplicationDbContext db;
        private readonly IMapper _mapper;

        public AutorServices(ApplicationDbContext db, IMapper mapper)
        {
            this.db = db;
            this._mapper = mapper;
        }

        public IEnumerable<Autor> GetAll() {
            IEnumerable<Autor> ListaAutores = new List<Autor>();
            try
            {
                ListaAutores = (from aut in db.Autor
                                select new Autor()
                                {
                                    Id = aut.Id,
                                    CiudadProcedencia = aut.CiudadProcedencia,
                                    CorreoElectronico = aut.CorreoElectronico,
                                    FechaNacimiento = aut.FechaNacimiento,
                                    NombreCompleto = aut.NombreCompleto
                                });
            }
            catch (Exception ex)
            {
                throw;
            }
            return ListaAutores;
        }

        public Autor GetByNombre(string nombre)
        {
            Autor autor = null;
            try
            {
                autor = (from aut in db.Autor
                         where aut.NombreCompleto.Equals(nombre)
                         select new Autor()
                         {
                             Id = aut.Id,
                             CiudadProcedencia = aut.CiudadProcedencia,
                             CorreoElectronico = aut.CorreoElectronico,
                             FechaNacimiento = aut.FechaNacimiento,
                             NombreCompleto = aut.NombreCompleto
                         }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
            return autor;
        }

        public async Task<Response> Add(Autor autor) {
            Response save = null;
            try
            {
                if (db.Autor.Where(x => x.CorreoElectronico.Equals(autor.CorreoElectronico.Trim())).Any())
                    return new Response(false, "El correo electrónico ya se encuentra registrado.");

                Models.Entities.Autor aut = new Models.Entities.Autor();
                var xxx = _mapper.Map(autor, aut);
                aut.Id = 0;
                db.Autor.Add(aut);
                if (await db.SaveChangesAsync() > 0) save = new Response(true, "Se guardo la información exitosamente.");
                else save = new Response(false, "No se guardo la información.");
            }
            catch (Exception ex)
            {
                save = new Response(false, "Ocurrio un error guardando la información.");
            }
            return save;
        }




    }
}

﻿using ApiNexos.Context;
using ApiNexos.Interfaces;
using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Services
{
    public class EditorialServices : IEditorialServices
    {
        private readonly ApplicationDbContext db;
        private readonly IMapper _mapper;

        public EditorialServices(ApplicationDbContext db, IMapper mapper)
        {
            this.db = db;
            _mapper = mapper;
        }

        public IEnumerable<Editorial> GetAll()
        {
            IEnumerable<Editorial> ListaAutores = new List<Editorial>();
            try
            {
                ListaAutores = (from aut in db.Editorial
                                select new Editorial()
                                {
                                    Id = aut.Id,
                                    Nombre = aut.Nombre,
                                    DireccionCorrespondencia = aut.DireccionCorrespondencia,
                                    Telefono = aut.Telefono,
                                    CorreoElectronico = aut.CorreoElectronico,
                                    MaximoLibrosRegistrados = aut.MaximoLibrosRegistrados
                                });
            }
            catch (Exception ex)
            {
                throw;
            }
            return ListaAutores;
        }

        public Editorial GetByNombre(string nombre)
        {
            Editorial autor = null;
            try
            {
                autor = (from edi in db.Editorial
                         where edi.Nombre.Equals(nombre)
                         select new Editorial()
                         {
                             Id = edi.Id,
                             Nombre = edi.Nombre,
                             DireccionCorrespondencia = edi.DireccionCorrespondencia,
                             Telefono = edi.Telefono,
                             CorreoElectronico = edi.CorreoElectronico,
                             MaximoLibrosRegistrados = edi.MaximoLibrosRegistrados
                         }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
            return autor;
        }

        public async Task<Response> Add(Editorial editorial)
        {
            Response save = null;
            try
            {
                Models.Entities.Editorial Edi = new Models.Entities.Editorial();
                _mapper.Map(editorial, Edi);
                if (Edi.MaximoLibrosRegistrados <= 0) Edi.MaximoLibrosRegistrados = -1;
                db.Editorial.Add(Edi);
                if (await db.SaveChangesAsync() > 0) save = new Response(true, "Se guardo la información exitosamente.");
                else save = new Response(false, "No se guardo la información.");
            }
            catch (Exception ex)
            {
                save = new Response(false, "Ocurrio un error guardando la información.");
            }
            return save;
        }

    }
}

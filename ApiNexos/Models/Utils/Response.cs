﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Models.Utils
{
    public class Response
    {
        public bool estado { get; set; }
        public string mensaje { get; set; }
        public object data { get; set; }

        public Response(bool estado, string mensaje)
        {
            this.estado = estado;
            this.mensaje = mensaje;
        }

        public Response(bool estado, string mensaje, object data)
        {
            this.estado = estado;
            this.mensaje = mensaje;
            this.data = data;
        }

    }
}

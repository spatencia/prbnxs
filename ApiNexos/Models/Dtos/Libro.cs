﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Models.Dtos
{
	public class Libro
	{
		public int Id { get; set; }
		public string Titulo { get; set; }
		public int Anio { get; set; }
		public string Genero { get; set; }
		public string NumeroPaginas { get; set; }
		public int IdEditorial { get; set; }
		public string NombreEditorial { get; set; }
		public int IdAutor { get; set; }
		public string NombreAutor { get; set; }

		public Libro()
		{
		}
	}
}

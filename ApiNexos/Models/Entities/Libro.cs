﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Models.Entities
{
	public class Libro
	{
		public int Id { get; set; }
		public string Titulo { get; set; }
		public int Anio { get; set; }
		public string Genero { get; set; }
		public string NumeroPaginas { get; set; }
		public int IdEditorial { get; set; }
		public int IdAutor { get; set; }


		public Libro()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Models.Entities
{
	public class Editorial
	{
		public int Id { get; set; }
		public string Nombre { get; set; }
		public string DireccionCorrespondencia { get; set; }
		public string Telefono { get; set; }
		public string CorreoElectronico { get; set; }
		public int MaximoLibrosRegistrados { get; set; }
        public Editorial()
        {
        }
    }
}

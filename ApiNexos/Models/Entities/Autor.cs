﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Models.Entities
{
	public class Autor
	{
		public int Id { get; set; }
		public string NombreCompleto { get; set; }
		public DateTime FechaNacimiento { get; set; }
		public string CiudadProcedencia { get; set; }
		public string CorreoElectronico { get; set; }
	}

}

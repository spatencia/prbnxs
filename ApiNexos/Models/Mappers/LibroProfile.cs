﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Models.Mappers
{
    public class LibroProfile : Profile
    {
        public LibroProfile() {
            CreateMap<Dtos.Libro, Entities.Libro>();
        }

    }
}

﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Models.Mappers
{
    public class AutorProfile : Profile
    {
        public AutorProfile() {
            CreateMap<Dtos.Autor, Entities.Autor>();
        }

    }
}

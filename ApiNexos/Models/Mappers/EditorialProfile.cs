﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Models.Mappers
{
    public class EditorialProfile : Profile
    {
        public EditorialProfile() {
            CreateMap<Dtos.Editorial, Entities.Editorial>();
        }

    }
}

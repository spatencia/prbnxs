﻿using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Interfaces
{
    public interface IEditorialServices
    {
        public IEnumerable<Editorial> GetAll();
        public Editorial GetByNombre(string nombre);
        Task<Response> Add(Editorial autor);
    }
}

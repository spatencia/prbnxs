﻿using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Interfaces
{
    public interface ILibroServices
    {
        public IEnumerable<Libro> GetAll();
        public IEnumerable<Libro> GetLibrosByAutorTituloAnio(string buscar);
        Task<Response> Add(Libro autor);
    }
}

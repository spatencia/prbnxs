﻿using ApiNexos.Models.Dtos;
using ApiNexos.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNexos.Interfaces
{
    public interface IAutorServices
    {
        public IEnumerable<Autor> GetAll();
        public Autor GetByNombre(string nombre);
        Task<Response> Add(Autor autor);
    }
}

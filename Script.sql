/*
create table dbo.Autor (
Id int primary key identity(1,1),
NombreCompleto varchar(50) not null,
FechaNacimiento date,
CiudadProcedencia varchar(50),
CorreoElectrónico varchar(50)
);

create table dbo.Editorial (
Id int primary key identity(1,1),
Nombre varchar(50),
DireccionCorrespondencia varchar(50),
Telefono varchar(15),
CorreoElectronico varchar(50),
MaximoLibrosRegistrados int
);

create table dbo.Libro (
Id int primary key identity(1,1),
Titulo varchar(50) not null,
Anio int not null,
Genero varchar(50) not null,
NumeroPaginas varchar(50) not null,
IdEditorial int FOREIGN KEY REFERENCES Editorial(Id),
IdAutor int FOREIGN KEY REFERENCES Autor(Id)
);
*/

delete from dbo.Libro;
delete from dbo.Autor;
delete from dbo.Editorial;


select * from dbo.Autor;
select * from dbo.Editorial;
select * from dbo.Libro;
